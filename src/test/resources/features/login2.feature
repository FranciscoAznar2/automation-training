Feature: Check the correct behavior of the login features

  Scenario:  SC02.Check that the user sets the correct credentials,
  the app responses with an success feedback.
    Given I go to 'http://automationpractice.com'
    When I try to login with my credentials
    Then I should see the following message:
    """
    Authentication success.
    """