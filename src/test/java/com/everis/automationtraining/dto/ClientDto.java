package com.everis.automationtraining.dto;

public class ClientDto {

    private String email;
    private String password;

    public ClientDto(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
