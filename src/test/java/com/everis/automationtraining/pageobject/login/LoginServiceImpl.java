package com.everis.automationtraining.pageobject.login;

import com.everis.automationtraining.dto.ClientDto;
import com.everis.automationtraining.util.BrowserDriver;
import com.everis.automationtraining.util.WaitUtils;
import org.openqa.selenium.support.PageFactory;

public class LoginServiceImpl implements LoginService {

    private LoginPage loginPage;

    public LoginServiceImpl() {
        loginPage = PageFactory.initElements(BrowserDriver.getCurrentDriver(), LoginPage.class);
    }

    @Override
    public void doLogin(ClientDto clientDto) {
        setEmail(clientDto.getEmail());
        setPassword(clientDto.getPassword());
        submit();
    }

    @Override
    public String getFeedback() {
        return getAlertText();
    }

    private void setEmail(String email) {
        WaitUtils.waitUntilVisible(loginPage.getEmailField(), 10);
        loginPage.getEmailField().sendKeys(email);
    }

    private void setPassword(String password) {
        loginPage.getPasswordField().sendKeys(password);
    }

    private void submit() {
        loginPage.getSubmitBtn().click();
    }

    private String getAlertText() {
        WaitUtils.waitUntilVisible(loginPage.getAlertMessage(), 10);
        return loginPage.getAlertMessage().getText();
    }

}
