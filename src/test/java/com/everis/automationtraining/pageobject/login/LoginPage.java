package com.everis.automationtraining.pageobject.login;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "passwd")
    private WebElement passwordField;

    @FindBy(id = "SubmitLogin")
    private WebElement submitBtn;

    @FindBy(css = "div.alert.alert-danger > ol > li")
    private WebElement alertMessage;

    public WebElement getEmailField() {
        return emailField;
    }

    public WebElement getPasswordField() {
        return passwordField;
    }

    public WebElement getSubmitBtn() {
        return submitBtn;
    }

    public WebElement getAlertMessage() {
        return alertMessage;
    }
}
