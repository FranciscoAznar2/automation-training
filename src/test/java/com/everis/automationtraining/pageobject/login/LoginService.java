package com.everis.automationtraining.pageobject.login;

import com.everis.automationtraining.dto.ClientDto;

public interface LoginService {

    void doLogin(ClientDto credentials);

    String getFeedback();

}
