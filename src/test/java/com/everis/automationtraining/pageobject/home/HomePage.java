package com.everis.automationtraining.pageobject.home;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage {

    @FindBy(css = "#header > div.nav > div > div > nav > div.header_user_info > a")
    private WebElement signInBtn;

    public WebElement getSignInBtn() {
        return signInBtn;
    }

}
