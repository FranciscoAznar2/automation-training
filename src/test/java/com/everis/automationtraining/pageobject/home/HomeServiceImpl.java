package com.everis.automationtraining.pageobject.home;

import com.everis.automationtraining.util.BrowserDriver;
import com.everis.automationtraining.util.WaitUtils;
import org.openqa.selenium.support.PageFactory;

public class HomeServiceImpl implements HomeService {

    private HomePage homePage;

    public HomeServiceImpl() {
        homePage = PageFactory.initElements(BrowserDriver.getCurrentDriver(), HomePage.class);
    }

    @Override
    public void goToSignInPage() {
        clickSignIn();
    }

    private void clickSignIn() {
        WaitUtils.waitUntilClickable(homePage.getSignInBtn(), 10);
        homePage.getSignInBtn().click();
    }

}
