package com.everis.automationtraining.pageobject.home;

public interface HomeService {

    void goToSignInPage();

}
