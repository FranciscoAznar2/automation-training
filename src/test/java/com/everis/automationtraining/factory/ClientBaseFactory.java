package com.everis.automationtraining.factory;

import com.everis.automationtraining.dto.ClientDto;

public abstract class ClientBaseFactory {

    public abstract ClientDto build();

}
