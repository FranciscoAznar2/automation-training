package com.everis.automationtraining.factory;

import com.everis.automationtraining.dto.ClientDto;
import org.apache.commons.lang3.RandomStringUtils;

public class ClientFactory extends ClientBaseFactory {

    @Override
    public ClientDto build() {
        return new ClientDto(
                buildEmail(),
                buildPassword()
        );
    }

    private String buildEmail() {
        return RandomStringUtils.randomAlphabetic(5) + "@automationtraining.com";
    }

    private String buildPassword() {
        return RandomStringUtils.randomAlphabetic(6);
    }

}
