package com.everis.automationtraining.util;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitUtils {

    private static WebDriverWait waitDriver(int seconds) {
        return new WebDriverWait(BrowserDriver.getCurrentDriver(), seconds);
    }

    public static void waitUntilClickable(WebElement element, int seconds) {
        waitDriver(seconds).until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitUntilVisible(WebElement element, int seconds) {
        waitDriver(seconds).until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitUntilInvisible(WebElement element, int seconds) {
        waitDriver(seconds).until(ExpectedConditions.invisibilityOf(element));
    }

    public static void waitTextElementPresent(WebElement element, int seconds, String text) {
        waitDriver(seconds).until(ExpectedConditions.textToBePresentInElement(element, text));
    }
}
