package com.everis.automationtraining.util;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class BrowserDriver {

    private static WebDriver mDriver;

    public static WebDriver getCurrentDriver() {

        if (mDriver == null) {
            initDriver();
        } else if (mDriver instanceof ChromeDriver && ((ChromeDriver) mDriver).getSessionId() == null) {
            initDriver();
        }

        return mDriver;
    }

    private static void initDriver() {
        ChromeOptions chromeOptions = new ChromeOptions();
        WebDriverManager.chromedriver().setup();
        mDriver = new ChromeDriver(chromeOptions);
    }

    public static void maximizeBrowser() {
        mDriver.manage().window().maximize();
    }

    public static void closeBrowser() {
        mDriver.quit();
    }
}