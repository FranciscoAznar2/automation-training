package com.everis.automationtraining.util;

import cucumber.api.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public class WebDriverUtils {

    public static Set<Cookie> getCookies() {
        return BrowserDriver.getCurrentDriver().manage().getCookies();
    }

    public static Object executeJavascriptCommand(String command) {
        return ((JavascriptExecutor) BrowserDriver.getCurrentDriver()).executeScript(command);
    }

    public static void switchToDefaultConten() {
        BrowserDriver.getCurrentDriver().switchTo().defaultContent();
    }

    public static void switchFrameByElement(WebElement webElement) {
        BrowserDriver.getCurrentDriver().switchTo().frame(webElement);
    }

    public static void switchFrameByIndex(int count) {
        BrowserDriver.getCurrentDriver().switchTo().frame(count);
    }

    public static void takeScreenshot(Scenario scenario) throws IOException {
        File scrFile = ((TakesScreenshot) BrowserDriver.getCurrentDriver()).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "/target/report/" + scenario.getName() + ".png"));
        attachScreenshotReport(scenario, scrFile);
    }

    private static void attachScreenshotReport(Scenario scenario, File scrFile) throws IOException {
        byte[] data = FileUtils.readFileToByteArray(scrFile);
        scenario.embed(data, "image/png");
    }

}