package com.everis.automationtraining.step;

import com.everis.automationtraining.dto.ClientDto;
import com.everis.automationtraining.factory.ClientFactory;
import com.everis.automationtraining.pageobject.home.HomeService;
import com.everis.automationtraining.pageobject.home.HomeServiceImpl;
import com.everis.automationtraining.pageobject.login.LoginService;
import com.everis.automationtraining.pageobject.login.LoginServiceImpl;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import static com.everis.automationtraining.util.BrowserDriver.getCurrentDriver;

public class LoginSteps {

    private HomeService homeService;
    private LoginService loginService;
    private ClientFactory clientFactory;

    private WebDriver driver;

    public LoginSteps() {
        this.driver = getCurrentDriver();
        this.homeService = new HomeServiceImpl();
        this.loginService = new LoginServiceImpl();
        this.clientFactory = new ClientFactory();
    }

    @Given("^I go to '(.*)'$")
    public void iGoToPage(String url) throws Throwable {
        driver.get(url);
    }

    @When("^I try to login with my credentials$")
    public void iTryToLoginWithTheFollowingCredentials() throws Throwable {
        ClientDto clientDto = clientFactory.build();
        homeService.goToSignInPage();
        loginService.doLogin(clientDto);
    }

    @Then("^I (should|shouldn't) see the following message:$")
    public void iShouldSeeTheFollowingMessage(String condition, String message) throws Throwable {

        if ("should".equals(condition)) {
            Assert.assertTrue(message.contains(loginService.getFeedback()));
        } else {
            Assert.assertFalse(message.contains(loginService.getFeedback()));
        }

    }
}


