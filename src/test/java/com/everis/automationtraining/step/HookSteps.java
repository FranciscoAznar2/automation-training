package com.everis.automationtraining.step;

import com.everis.automationtraining.util.BrowserDriver;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import static com.everis.automationtraining.util.WebDriverUtils.takeScreenshot;

public class HookSteps {

    @Before
    public void openBrowser() {
        try {
            BrowserDriver.getCurrentDriver();
            BrowserDriver.maximizeBrowser();
        } catch (RuntimeException ignore) {
            ignore.getCause();
        }
    }

    @After
    public void closeBrowser() {
        BrowserDriver.closeBrowser();
    }

    @After
    public void embedScreenshot(Scenario scenario) {
        try {
            takeScreenshot(scenario);
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            closeBrowser();
        }
    }

}
